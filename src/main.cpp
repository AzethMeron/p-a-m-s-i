#include "pamsi_tester/Tester.hpp"
#include "spdlog/spdlog.h"
#include <fstream>
#include <vector>
#include <string>

/* ************************************************************************************ */

std::string Command = "";
std::string sb = "sb";
std::string sq = "sq";
std::string sh = "sh";

/* ************************************************************************************ */

template<typename Type>
class Array
{
	private:
		void swap(Type& a, Type& b) const { Type c = a; a = b; b = c; }
		void workspace_quick_sort(const unsigned int& left, const unsigned int& right);
		unsigned int workspace_partition(const unsigned int& left, const unsigned int& right);
		void workspace_heapify(const unsigned int& Size);
		inline void workspace_check_node(const unsigned int& i);
		unsigned int workspace_to_parent(const unsigned int& i)	{ if(i%2) { return (i-1)/2; } else { return (i-2)/2; } }
		//unsigned int workspace_to_left_son(const unsigned int& i) { return 2*i+1; }
		//unsigned int workspace_to_right_son(const unsigned int& i) { return 2*i+2; }
	protected:
		std::vector<Type> values;
	public:
		unsigned int size(void) const { return values.size(); }
		Type& operator [] (const unsigned int& index)       { return values[index]; }
		Type  operator [] (const unsigned int& index) const { return values[index]; }
		Array(unsigned int Size) : values(Size) {}
		
		Array<Type> sort_bubble(void) const;
		Array<Type> sort_quick(void) const;
		Array<Type> sort_heap(void) const;
		Array<Type> reverse(void) const;
		
		Array(const Array<Type>&& temp) { this->values = temp.values; }
		Array(const Array<Type>& temp) { this->values = temp.values; }
		Array<Type>& operator = (const Array<Type>& temp) { values = temp.values; return *this; }
};

template<typename Type>
std::istream& operator >> (std::istream& input, Array<Type>& array)
{
	for(unsigned int i = 0; i < array.size(); i++)
	{
		input >> array[i];
	}
	return input;
}

template<typename Type>
std::ostream& operator << (std::ostream& output, Array<Type> array)
{
	for(unsigned int i = 0; i < array.size(); i++)
	{
		output << array[i] << " ";
	}
	return output;
}

template<typename Type>
Array<Type> Array<Type>::reverse(void) const
{
	const unsigned int Size = this->size();
	Array<Type> output(Size);
	for(unsigned int i = 0; i < Size; i++)
	{
		output[i] = (*this)[Size-1-i];
	}  
	return output;
}

/* ********************************* SORTOWANIA *************************************** */

template<typename Type>
Array<Type> Array<Type>::sort_bubble(void) const
{ 
	bool not_sorted = true;
	Array<Type> output = *this;
	while(not_sorted)
	{
		not_sorted = false;
		for(unsigned int i = 1; i < size(); i++)
		{
			if(output[i-1] < output[i])
			{
				not_sorted = true;
				swap(output[i-1],output[i]);
			}
		}
	}
	return output;
}

template<typename Type>
Array<Type> Array<Type>::sort_quick(void) const
{
	Array<Type> output = *this;
	output.workspace_quick_sort(0,size()-1);
	return output;
}

template<typename Type>
void Array<Type>::workspace_quick_sort(const unsigned int& left, const unsigned int& right)
{
	if(left < right)
	{
		unsigned int q = this->workspace_partition(left,right);
		this->workspace_quick_sort(left,q);
		this->workspace_quick_sort(q+1,right);
	}
}

template<typename Type>
unsigned int Array<Type>::workspace_partition(const unsigned int& left, const unsigned int& right)
{
	Type reference_point = (*this)[(left+right)/2];
	unsigned int l = left, r = right;
	while(true)
	{
		while((*this)[r] < reference_point)
		{
			r--;
			if(r<=left) break;
		}
		while((*this)[l] > reference_point)
		{
			l++;
			if(l>=right) break;
		}
		if(l < r)
		{
			swap((*this)[l],(*this)[r]);
			l++;
			r--;
		}
		else
		{
			return r;
		}
	}
}

template<typename Type>
Array<Type> Array<Type>::sort_heap(void) const
{
	Array<Type> output = *this;
	output.workspace_heapify(size());
	return output;
}

template<typename Type>
void Array<Type>::workspace_heapify(const unsigned int& Size)
{
	if(Size==1) return;
	for(unsigned int i = 1; i < Size; i++)
	{
		this->workspace_check_node(i);
	}
	swap(values[Size-1],values[0]);
	this->workspace_heapify(Size-1);
}

template<typename Type>
inline void Array<Type>::workspace_check_node(const unsigned int& i)
{
	bool in_progress = true;
	for(unsigned int j = i; in_progress; j = workspace_to_parent(j))
	{
		in_progress = false;
		if(j>0)
		{
			if(values[j] < values[workspace_to_parent(j)])
			{
				swap(values[j],values[workspace_to_parent(j)]);
				in_progress = true;
			}
		}
	}
}

/* ************************************************************************************ */

template<typename Type>
class SortTester : public Tester <Array<Type>, Array<Type>>
{
	protected:
		Array<Type> runAlgorithm(const Array<Type>& inputData) override;
		Array<Type> readSingleInput(std::istream& inputStream) override;
};

template<typename Type>
Array<Type> SortTester<Type>::runAlgorithm(const Array<Type>& inputData)
{
	Array<Type> output(1);
	if(Command == sh)
	{
		output = inputData.sort_heap();
	}
	else if(Command == sq)
	{
		output = inputData.sort_quick();
	}
	else if(Command == sb)
	{
		output = inputData.sort_bubble();
	}
	return output;
}

template<typename Type>
Array<Type> SortTester<Type>::readSingleInput(std::istream& inputStream)
{
	unsigned int Size = 0;
	inputStream >> Size; 
	Array<Type> array(Size);
	inputStream >> array;
	return array;
}

/* ************************************************************************************ */

int main(int argc, char* argv[])
{
	if(argc!=3)
	{
		std::cout << std::endl << "Insufficent arguments" << std::endl;
		std::cout << "Argument 1: input file name (char*)" << std::endl;
		std::cout << "Argument 2: type of sort (sb, sq, sh) (char*)" << std::endl;
		std::cout << "sb - bubble sort, sq - quick sort, sh - heap sort" << std::endl;
	}
	else
	{
		std::ifstream inputFile(argv[1]);
		Command = argv[2];
		std::ofstream dataOutputFile{"output.txt"}, timeOutputFile{"times.csv"};
		if(!inputFile)
		{
			spdlog::error("input.txt cannot be opened!");
			return -1;
		}
		
		SortTester<int> tester;
		tester.runAllTests(inputFile, dataOutputFile, timeOutputFile);
	}
	std::cout << std::endl;
	return 0;
}
