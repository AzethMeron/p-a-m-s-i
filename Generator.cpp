#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <string>

inline int Loss(const int& min, const int& max)
{
	return ((rand()%(max-min+1))+min);
}

int main(int argc, char **argv)
{
	srand(time(NULL));
	if(argc!=5)
	{
		std::cout << std::endl << "Insufficent arguments" << std::endl;
		std::cout << "Argument 1: Amount of randomly generated numbers (int)" << std::endl;
		std::cout << "Argument 2: Minimal value (int)" << std::endl;
		std::cout << "Argument 3: Maximal value (int)" << std::endl;
		std::cout << "Argument 4: How many lines you want to generate (int)" << std::endl;
		std::cout << "Example: ./Generator 100 1 9 1" << std::endl;
	}
	else
	{
		const int size = atoi(argv[1]);
		const int min = atoi(argv[2]);
		const int max = atoi(argv[3]);
		const int times = atoi(argv[4]);
		
		std::string name = argv[1];
		std::string ext = ".txt";
		name = name + ext;
		
		std::ofstream output;
		output.open(name.c_str());
		if( output.good())
		{
			for(int j = 0; j < times; j++)
			{
				output << size << " ";
				for(int i = 0; i < size; i++)
				{
					output << Loss(min,max) << " ";
				}
				output << std::endl;
			}
			output.close();
			std::cout << "Generating has been successful" <<std::endl;
			return 0;
		}
		else
		{
			std::cout << std::endl << "Couldn't open file. Name: " << name << std::endl;
		}
	}
	std::cout << std::endl;
	return 1;
}
